@extends('layouts.sidebar')

@section("titulo") 
    <h1 class="m-0 text-dark">Administración de franquicias</h1>
@endsection	

@section("migajas_pan") 
    <li class="breadcrumb-item "><a href="/home">Inicio</a></li>
    <li class="breadcrumb-item active"><a href="#">Franquicias</a></li>
@endsection	



@section('contenido')
<div class="container" >
                   <!-- Row -->
                   <div class="row">
                    <div class="col-xl-12">
                            <div class="row">
                                <div class="col-sm">
									<div class="row justify-content-center">
										<div class="col-sm-3">
											<button id="" class="btn-modal-new-producto btn btn-success btn-block ">
												<i class="fas fa-plus"></i>
												Nueva franquicia
											</button>
										</div>
									</div>
                                    <div class="row my-4">
                                        <div class="col-sm-12">
                                            <!-- contenedor del datatable -->
                                           <div class="table-wrap">
                                           </div>
                                           <!-- contenedor del datatable -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->
              

</div> <!-- end container -->





<!--------------------- MODAL EDITAR ------------------------------>
<div class="modal fade bd-example-modal-lg" id="modal_editar_producto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Editar franquicia </h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>

		<div class="modal-body">
		<!--<form method="POST" action="">-->
			<input type="hidden" value="" id="id_curso">
			@csrf															
			@if ($errors->has('fullname'))
			<div class="form-group row has-danger ">
			@else
			<div class="form-group row">
			@endif
			<label class="col-sm-12 col-form-label">Nombre de la empresa:<span class="required">*</span></label>
				<div class="col-sm-12">
					<input type="text"  class="form-control validacion_update"  id="i_nombre_edit" maxlength="150">
					<p class="col-form-label">{{ $errors->first('fullname') }}</p>
				</div>
			</div>


			<h6>Datos del Franquiciador</h6>
			<div class="form-group">
				<label for="inputAddress">Nombre:</label>
				<input type="text" class="form-control validacion_update"   id="i_edit_user_name" maxlength="100" placeholder="Nombre">
			</div>

			<div class="form-row">
				<div class="form-group col-md-6">
				<label for="inputEmail4">Apellido paterno:</label>
				<input type="text" class="form-control validacion_update"  id="i_edit_user_ap1" maxlength="100" placeholder="Apellido paterno">
				</div>
				<div class="form-group col-md-6">
				<label for="inputPassword4">Apellido materno:</label>
				<input type="text" class="form-control validacion_update"   id="i_edit_user_ap2" maxlength="100" placeholder="Apellido materno">
				</div>
			</div>
			<div class="form-row">
				<div class="form-group col-md-6">
					<label for="inputAddress2">Correo  electronico:</label>
					<input type="email" class="form-control validacion_update"  id="i_edit_user_mail" placeholder="electronico">
				</div>

				<div class="form-group col-md-6">
					<label for="inputAddress2">Contraseña</label>
					<input type="password" class="form-control "  id="i_edit_user_password" placeholder="electronico">
				</div>
			</div>

			<div class="form-group row justify-content-center text-center d-none" id="i_alert_validacion_edit">
				<div class="alert alert-danger col-md-12" role="alert" style="color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;">
					Asegúrese que los campos <a class="alert-link">marcados</a> estén rellenados!
				</div>
			</div>


                                                           
		<!--</form>-->

		</div><!--modal-body-->
		<div class="modal-footer">
			<a class="btn text-white btn-secondary btn-cancel" data-dismiss="modal">Cancelar</a>
			<input type="button" class="btn btn-primary" value="Actualizar" id="btn_guardar_edit_producto">
		</div>
		</div>
	</div>
</div>
<!-----------------------END MODAL EDITAR ---------------------------->



<!----------------------- MODAL NUEVO ---------------------------->
<div class="modal fade bd-example-modal-lg" id="modal_clase_usuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> 
		<div class="modal-dialog modal-lg" >
			<div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title text-center" style="text-align: center;" id="exampleModalLabel">Crear nueva franquicia </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- comienza bodal body -->
                <div class="modal-body">

                    <!--<form method="POST" action="">-->

                        <input type="hidden" value="" id="id_curso">
                        @csrf															
                        @if ($errors->has('fullname'))
                        <div class="form-group row has-danger ">
                        @else
                        <div class="form-group row">
                        @endif
                        <label class="col-sm-12 col-form-label">Nombre de la franquicia:</label>
                            <div class="col-sm-12 verifica-valid" id="vvn-1">
                                <input type="text"  class="form-control validacion_create" name='inombre' id="inombre" maxlength="150" placeholder="Nombre de la franquicia">
                                <p class="col-form-label">{{ $errors->first('fullname') }}</p>
                            </div>
						</div>

                        <h6>Datos del Franquiciador</h6>
                        <div class="form-group">
                            <label for="inputAddress">Nombre:</label>
                            <input type="text" class="form-control validacion_create"   id="i_user_name" maxlength="100" placeholder="Nombre">
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                            <label for="inputEmail4">Apellido paterno:</label>
                            <input type="text" class="form-control validacion_create"  id="i_user_ap1" maxlength="100" placeholder="Apellido paterno">
                            </div>
                            <div class="form-group col-md-6">
                            <label for="inputPassword4">Apellido materno:</label>
                            <input type="text" class="form-control validacion_create"   id="i_user_ap2" maxlength="100" placeholder="Apellido materno">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputAddress2">Correo  electronico:</label>
                                <input type="email" class="form-control validacion_create"  id="i_user_mail" placeholder="electronico">
                            </div>

							<div class="form-group col-md-6">
                                <label for="inputAddress2">Contraseña</label>
                                <input type="password" class="form-control validacion_create"  id="i_user_password" placeholder="electronico">
                            </div>
						</div>


						<div class="form-group row justify-content-center text-center d-none" id="i_alert_validacion_create">
							<div class="alert alert-danger col-md-12" role="alert" style="color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;">
								Asegúrese que los campos <a class="alert-link">marcados</a> estén rellenados!
							</div>
						</div>                    
               
					
                    <!--</form>-->
                </div>
                <!-- endcomienza bodal body -->
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<input type="button" class="btn btn-primary btn_guardar_new_producto" value="Guardar">
				<!-- <a type="button" class="btn btn-info" id="btn_guardar_cambios_class_estud">Guardar cambios</a> -->
			</div>
			</div>
		</div>
</div>
<!----------------------- END MODAL NUEVO ---------------------------->
@endsection	



@section('adicional_js')
<script src="{{asset('js/franquicias/franquicias_view.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection	

