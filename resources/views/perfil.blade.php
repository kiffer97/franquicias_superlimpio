@extends('layouts.sidebar')

@section("titulo") 
    <h1 class="m-0 text-dark">Perfil</h1>
@endsection	

@section("migajas_pan") 
    <li class="breadcrumb-item "><a href="/home">Inicio</a></li>
    <li class="breadcrumb-item "><a href="#">Perfil de usuario</a></li>
@endsection	



@section('contenido')
<section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle i_imagen_perfil_principal"
                       src="/images/profile_user/<?php echo "/".Auth::user()->profile_picture?>"
                       alt="User profile picture" style="width:100px;height:100px">
                </div>

                <h3 class="profile-username text-center text-uppercase"><?php echo Auth::user()->fullname?></h3>
                <p class="text-muted text-center i_rol"></p>
                <!-- <form action=""> -->
                
                  <!-- <input type="file" name="" id=""> -->

                  <button class="btn btn-primary btn-block btn_abrir_modal_imagen" type="submit">Actualizar</button>
                
                <!-- </form> -->
            
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->



          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Información</a></li>
                  <!-- <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Timeline</a></li> -->
                  <!-- <li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">Settings</a></li> -->
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">

                  <div class="active tab-pane" id="settings">
                    <!-- <form class="form-horizontal"> -->

                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Nombre</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control validacion_update" id="i_nombre" placeholder="Nombre completo">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputName" class="col-sm-2 col-form-label">Apellido Paterno</label>
                        <div class="col-sm-4">
                          <input type="email" class="form-control validacion_update" id="i_firstname" placeholder="Apellido Paterno">
                        </div>

                        <label for="inputName" class="col-sm-2 col-form-label">Apellido Materno</label>
                        <div class="col-sm-4">
                          <input type="email" class="form-control validacion_update" id="i_lastname" placeholder="Apellido Materno">
                        </div>
                      </div>

                      <div class="form-group row">
                        <label for="inputEmail" class="col-sm-2 col-form-label">Correo</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" readonly id="i_mail" placeholder="Correo">
                        </div>
                      </div>


                      <div class="form-group row">
                        <label for="inputSkills" class="col-sm-2 col-form-label">Contraseña</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" id="i_password" placeholder="Contraseña">
                        </div>
                      </div>
                      
                      <div class="form-group row justify-content-center text-center d-none" id="i_alert_campos_vacios1">
                        <div class="alert alert-danger col-md-12" role="alert" style="color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;">
                          Asegúrese que los campos <a class="alert-link">marcados</a> estén rellenados!
                        </div>
                      </div>

                      <div class="form-group row ">
                        <div class=" col-sm-3">
                        <!-- offset-sm-2 -->
                          <button class="btn btn-primary btn-block btn_update_perfil">Actualizar</button>
                        </div>
                      </div>
                    <!-- </form> -->
                  </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


<!-- subir imagen modal -->
<!-- Modal -->
<div class="modal fade" id="i_subir_imagen_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Elija una imagen de perfil</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body " >
        <form method="POST" enctype="multipart/form-data" id="i_form_subir_imagen">
        @csrf															
          <input type="file" name="imagen_perfil" id="imagen_perfil" accept="image/png, image/jpeg, image/jpg">

          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            <button type="submit" class="btn btn-primary">Guardar</button>
          </div>

        </form>
      </div>

    </div>
  </div>
</div>
<!-- subir imagen modal -->
@endsection	



@section('adicional_js')
<script src="{{asset('js/usuario/usuario.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection	

