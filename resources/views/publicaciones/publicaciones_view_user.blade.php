@extends('layouts.sidebar')

@section("titulo") 
    <h1 class="m-0 text-dark">Publicaciones</h1>
@endsection	

@section("migajas_pan") 
    <li class="breadcrumb-item "><a href="/home">Inicio</a></li>
    <li class="breadcrumb-item active"><a href="#">Publicaciones</a></li>
@endsection	



@section('contenido')
<div class="container" >
<!-- Row -->
<div class="row">
	<div class="col-xl-12">
		<div class="row">

			<!-- <div class="col-md-12 ">
				<div class="row justify-content-center">
					<div class="col-md-2">
						<button id="" class="btn-modal-new-producto btn-block btn btn-success ">
							<i class="fas fa-plus"></i>
							Nueva publicación
						</button>
					</div>
				</div> -->

				<div class="col-md-12 my-3">
					<!-- contenedor del datatable -->
					<div class="table-wrap">
					</div>
					<!-- contenedor del datatable -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /Row -->


</div> <!-- end container -->




<!--------------------- MODAL ACTUALIZAR ------------------------------>

<div class="modal fade bd-example-modal-lg" id="modal_editar_producto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Editar publicación </h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
		<form method="POST" enctype="multipart/form-data"  id="i_form_edit_type_file">
			<input type="hidden" value="" id="id_curso" 	 name="id_curso">
			<input type="hidden" value="" id="eliminar_file" name="eliminar_file">
			@csrf															
			@if ($errors->has('fullname'))
			<div class="form-group row has-danger ">
			@else
			<div class="form-group row">
			@endif
			<label class="col-sm-2 col-form-label">Nombre de la publicación:<span class="required">*</span></label>
				<div class="col-sm-10">
					<input type="text"  class="form-control validacion_edit"  id="i_nombre_edit" name="i_nombre_edit" maxlength="100">
					<p class="col-form-label">{{ $errors->first('fullname') }}</p>
				</div>
			</div>

			@if ($errors->has('fullname'))
			<div class="form-group row has-danger ">
			@else
			<div class="form-group row">
			@endif
			<label class="col-sm-2 col-form-label">Instrucciones:<span class="required">*</span></label>
				<div class="col-sm-10">
					<textarea class="form-control validacion_edit" name="idescripcion_edit" id="idescripcion_edit" cols="30" rows="5" maxlength="250"></textarea>
					<p class="col-form-label">{{ $errors->first('fullname') }}</p>
				</div>
			</div>

			<div class="form-group row">
				<label  class="col-sm-2 col-form-label">Fecha limite</label>
				<div class="col-sm-10 verifica-valid" id="vvn-1">
					<input type="date" name="i_edit_fecha_limite" id="i_edit_fecha_limite" class="form-control validacion_edit">
				</div>
			</div>

			<div class="form-group row">
				<label  class="col-sm-2 col-form-label">Enlace url:</label>
				<div class="col-sm-10 verifica-valid" id="vvn-1">
					<input type="url" name="i_edit_enlace" id="i_edit_enlace" class="form-control">
				</div>
			</div>

			<div class="form-group row d-none " id="i_mostrar_input_file">
				<label  class="col-sm-2 col-form-label">Material adicional: </label>
				<div class="col-sm-10 verifica-valid " >
					<input type="file" name="i_edit_file" id="i_edit_file" class="mt-2">
				</div>
			</div>

			<div class="form-group row " id="contenedor_file_eliminar2">
			</div>

			<div class="form-group row justify-content-center text-center d-none" id="i_alert_campos_vacios">
				<div class="alert alert-danger col-md-12" role="alert" style="color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;">
					Asegúrese que los campos <a class="alert-link">marcados</a> estén rellenados!
				</div>
			</div>

			
		
			<div class="row justify-content-end">
				<a class="btn text-white btn-secondary btn-cancel ml-1 mr-1" data-dismiss="modal">Cancelar</a>
				<input type="submit" class="btn btn-primary ml-1 mr-1 " value="Actualizar" > <!--btn_guardar_edit_producto-->
			</div>

			
		</form>

		</div>
		<div class="modal-footer">
			<!-- <a class="btn text-white btn-danger btn-cancel" data-dismiss="modal">Cancelar</a>
			<input type="button" class="btn btn-info" value="Actualizar" id="btn_guardar_edit_producto"> -->
		</div>
		</div>
	</div>
</div>
<!-- --------------------------------------------------------------- -->



<!----------------------- MODAL COMENTARIO ---------------------------->
<div class="modal fade bd-example-modal-lg" id="modal_vista_publicacion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true"> 
	<div class="modal-dialog modal-lg" >
		<div class="modal-content">
			<!-- <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div> -->
			<!-- comienza bodal body -->
			<div class="modal-body">
				<div class="card-body" >
					<div class="tab-content">

						<div class="active tab-pane" >
							<!-- Post -->
							<div class="post">
								<div class="user-block">
									<img class="img-circle img-bordered-sm img_publicador" src="" alt="user image">
									<span class="username">
									<a href="#" class="i_publicador">Jonathan Burke Jr.</a>

									<!-- <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a> -->
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">&times;</span>
									</button>
									</span>
									<span class="description i_shared_time">Publicación compartida - 7:30 PM today</span>
								</div>
								<!-- /.user-block -->
									<div class="row justify-content-center text-center">
										<h5 class="title_publicacion" >Este es el titulo de l apublicacion</h5>
									</div>
								<p class="i_descricion_publicacion">
									Lorem ipsum represents a long-held tradition for designers,
									typographers and the like. Some people hate it and argue for
									its demise, but others ignore the hate as they create awesome
									tools to help create filler text for everyone from bacon lovers
									to Charlie Sheen fans.
								</p>
								<div class="row justify-content-center">
									<div class="col-md-8">
										<table class="table table-bordered" id="content_material">
												<!-- aqui va el contenido adicional del usuario -->
										</table>
									</div>
								</div>

								<p>
									<a href="#" class="link-black text-sm mr-2"><i class="far fa-comments mr-1"></i>Comentarios</a>
								</p>
								
								<div class="row" style="max-height:250px;overflow:auto">

									<div class="timeline timeline-inverse"  id="content_comentarios">
										<!-- aqui van los comentarios hechos por el usuario -->
									</div>


								</div><!--row comment-->

								<div class="form-group row my-3">
									<div class="col-md-10">
										<input class="form-control form-control-sm text_comment" maxlength="250" type="text" placeholder="Agrega un comentario">
									</div>
									<div class="col-md-2">
										<li class="btn btn-success btn-block btn-sm btn_agregar_nuevo_comentario" style="cursor:pointer">Comentar</li>
									</div>
								</div>
							</div>
							<!-- /.post -->

						</div><!-- /.tab-activity -->
					</div>
					<!-- /.tab-content -->
					
				</div><!-- /.card-body -->
					
			</div><!--modal body-->
			<!-- endcomienza bodal body -->
		<div class="modal-footer">

			<!-- <a type="button" class="btn btn-info" id="btn_guardar_cambios_class_estud">Guardar cambios</a> -->
		</div>
		</div>
	</div>
</div>
<!----------------------- END MODAL COMENTARIO --------------------->



<script>
	
</script>
@endsection	



@section('adicional_js')
<script src="{{asset('js/publicaciones/publicaciones_view_user.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
@endsection	

