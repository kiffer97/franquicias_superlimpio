<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema  | Franquicias Superlimpio</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="/images/super-limpio-logo.png">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{url('plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{url('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{url('dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page" 
      style="background-image:url('images/login_background3.jpeg');background-repeat: no-repeat;
	  background-attachment: fixed; 
	  background-size: 100% 100%;"
    >

    <div class="row justify-content-center">
      <div class="col-md-4">
          <img src="/images/super-limpio-logo.png" class="img-fluid" alt="Responsive image">
      </div>
    </div>
<div class="login-box">
  <div class="login-logo">
    <!-- <a class="text-dark" href=""><b>Sistema  de Franquicias Superlimpio</b></a> -->
  </div>
  <!-- /.login-logo -->
  <div class="card">
  <h3 class="text-center" >Sistema  de Franquicias Superlimpio</h3>
    <div class="card-body login-card-body">
      <p class="login-box-msg">Ingrese sus credenciales para acceder.</p>

      <form action="{{ route('login') }}" method="post">
         @csrf

        <div class="input-group mb-3">
          <!-- <input type="email" class="form-control" placeholder="Correo"> -->
          <input id="email" 
                type="email" 
                class="form-control @error('email') is-invalid @enderror" 
                name="email" value="{{ old('email') }}" 
                required autocomplete="email" 
                placeholder="Correo"
                autofocus
            >

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>

          @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

        </div>

        <div class="input-group mb-3">
          <!-- <input type="password" class="form-control" placeholder="Contraseña"> -->
          <input id="password" 
                type="password" 
                class="form-control @error('password') is-invalid @enderror" 
                name="password" 
                required 
                placeholder="Contraseña"
                autocomplete="current-password">

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
          @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
          @enderror
        </div>

        <div class="row my-3 mb-5 justify-content-center">
          <div class="col-10">
            <div class="icheck-primary">
              <!-- <input type="checkbox" id="remember"> -->
              <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

              <label for="remember">
                Mantener la sesión iniciada.
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-12 mt-3">
            <button type="submit" class="btn btn-primary btn-block">Iniciar Sesión</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

      <div class="social-auth-links text-center mb-3 d-none">
        <p class="">- OR -</p>
        <a href="#" class="btn btn-block btn-primary">
          <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
        </a>
        <a href="#" class="btn btn-block btn-danger">
          <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
        </a>
      </div>
      <!-- /.social-auth-links -->

      <p class="mb-1 d-none">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
      <p class="mb-0 d-none">
        <a href="register.html" class="text-center">Register a new membership</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{ url('plugins/jquery/jquery.min.js') }} "></script>
<!-- Bootstrap 4 -->
<script src="{{ url('plugins/bootstrap/js/bootstrap.bundle.min.js') }} "></script>
<!-- AdminLTE App -->
<script src="{{ url('dist/js/adminlte.min.js') }} "></script>

</body>
</html>
