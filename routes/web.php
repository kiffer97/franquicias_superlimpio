<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {return view('welcome');});
Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/perfil', 'HomeController@perfil_usuario')->name('perfil_usuario');
//usuario
Route::get('/usuario_info', 'HomeController@usuario_info')->name('usuario_info');
Route::get('/update_perfil', 'HomeController@update_perfil')->name('update_perfil');
Route::post('/update_imagen_perfil', 'HomeController@update_imagen_perfil')->name('update_imagen_perfil');


//franquicia
Route::get('/cambiar_nav', 'FranquiciaController@cambiar_nav')->name('cambiar_nav');
Route::get('/tipo_nav', 'FranquiciaController@tipo_nav')->name('tipo_nav');
Route::get('/franquicias', 'FranquiciaController@franquicias_view')->name('franquicias_view');

Route::get('/create_franquicia', 'FranquiciaController@create_franquicia')->name('create_franquicia');
Route::get('/update_franquicia', 'FranquiciaController@update_franquicia')->name('update_franquicia');
Route::get('/delete_franquicia', 'FranquiciaController@delete_franquicia')->name('delete_franquicia');
Route::get('/franquicia_data_table', 'FranquiciaController@franquicia_data_table')->name('franquicia_data_table');
Route::get('/datos_one_franquicia', 'FranquiciaController@datos_one_franquicia')->name('datos_one_franquicia');
Route::get('/exist_mail', 'FranquiciaController@exist_mail')->name('exist_mail');


//publicaciones
Route::get('/publicaciones', 'PublicacionesController@publicaciones_view')->name('publicaciones_view');
Route::get('/datatable_publicaciones', 'PublicacionesController@datatable_publicaciones')->name('datatable_publicaciones');
Route::post('/create_publicacion', 'PublicacionesController@create_publicacion')->name('create_publicacion');
Route::post('/update_publicacion', 'PublicacionesController@update_publicacion')->name('update_publicacion');
Route::get('/delete_publicacion', 'PublicacionesController@delete_publicacion')->name('delete_publicacion');
Route::get('/data_publicacion', 'PublicacionesController@data_publicacion')->name('data_publicacion');
//comentarios
Route::get('/create_coment_user', 'PublicacionesController@create_coment_user')->name('create_coment_user');
Route::get('/data_publicacion_with_comments', 'PublicacionesController@data_publicacion_with_comments')->name('data_publicacion_with_comments');
Route::get('/datatable_publicaciones_user', 'PublicacionesController@datatable_publicaciones_user')->name('datatable_publicaciones_user');
Route::get('/data_publicacion_with_comments_admin', 'PublicacionesController@data_publicacion_with_comments_admin')->name('data_publicacion_with_comments_admin');




