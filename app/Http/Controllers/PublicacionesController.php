<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
// use app\User;
use DB;
use App\User;
use App\Franquicia;
use App\Comentario;
use App\Publicaciones;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;


// use Illuminate\Http\Request;

class PublicacionesController extends Controller
{
    public function publicaciones_view(){
        if(Auth::user()->rol=="Administrador"){
            return view('publicaciones.publicaciones_view');
        }else if(Auth::user()->rol=="Franquiciador"){
            return view('publicaciones.publicaciones_view_user');
        }
    }
    public function tareas_entregadas($id_curso,$id_tarea){
        $data=array($id_curso,$id_tarea);
        return view('tareas.tarea_entregadas',compact('data'));
    }

    public function create_publicacion(Request $request){

        if($request->hasfile('i_material_adicional')){
            $file=$request->file('i_material_adicional');
            $nombre=time().".".$file->getClientOriginalExtension();
            $file->move(public_path().'/fichero_material_publicaciones',$nombre);
           $material_adicional=$nombre; // $usuario=User::where('id','=', $request->id_usuario)->update(["profile_img"=>$nombre]);
        }else{
           $material_adicional="";
        }

        $nuevaTarea= new Publicaciones();
        $nuevaTarea->titulo             =$request->inombre;
        $nuevaTarea->id_user            =Auth::user()->id;
        $nuevaTarea->instrucciones      =$request->idescripcion;
        $nuevaTarea->enlace             =$request->i_url;
        $nuevaTarea->material_adicional =$material_adicional;
        $nuevaTarea->fecha_entrega      =$request->i_fecha_limite;
        $nuevaTarea->status             =1;
        $nuevaTarea->save();

        return "bien";
    }
    public function update_publicacion(Request $request){
        // echo $request->eliminar_file;
        
        if($request->eliminar_file==1){

            if($request->hasfile('i_edit_file')){
                $file=$request->file('i_edit_file');
                $nombre=time().".".$file->getClientOriginalExtension();
                $file->move(public_path().'/fichero_maestros',$nombre);
                $tarea=$nombre; // $usuario=User::where('id','=', $request->id_usuario)->update(["profile_img"=>$nombre]);
            }else{
                $tarea="";
            }

            Publicaciones::where('id','=',$request->id_curso)->update([
                'titulo'                =>$request->i_nombre_edit,
                'instrucciones'         =>$request->idescripcion_edit,
                'material_adicional'    =>$tarea,
                'enlace'                =>$request->i_edit_enlace,
                'fecha_entrega'         =>$request->i_edit_fecha_limite
                ]);
                return "bien";  

        }else if($request->eliminar_file==0){

            if($request->hasfile('i_edit_file')){
                $file=$request->file('i_edit_file');
                $nombre=time().".".$file->getClientOriginalExtension();
                $file->move(public_path().'/fichero_maestros',$nombre);
                $tarea=$nombre; 

                Publicaciones::where('id','=',$request->id_curso)->update([
                    'titulo'                =>$request->i_nombre_edit,
                    'instrucciones'         =>$request->idescripcion_edit,
                    'enlace'                =>$request->i_edit_enlace,
                    'material_adicional'    =>$tarea,
                    'fecha_entrega'         =>$request->i_edit_fecha_limite
                    ]);
                    return "bien";  
            }else{
                Publicaciones::where('id','=',$request->id_curso)->update([
                    'titulo'                =>$request->i_nombre_edit,
                    'instrucciones'         =>$request->idescripcion_edit,
                    'enlace'                =>$request->i_edit_enlace,
                    'fecha_entrega'         =>$request->i_edit_fecha_limite
                    ]);
                    return "bien";  
            }

           
        }
         
    }
    public function delete_publicacion(Request $request){
        Publicaciones::where('id','=',$request->id)->update(["status"=>0 ]);
        return "bien";                                        
    }
    public function datatable_publicaciones(){
        $datos=Publicaciones::where('status','=',1)
                            // ->where('id_curso','=',$id_curso)
                            ->get();
        return Datatables::of($datos)->make(true);
    }
    public function datatable_publicaciones_user(){
        $datos=DB::SELECT("SELECT 
        p.id,
        u.fullname AS publicador_name,
        u.profile_picture,
        p.titulo,
        p.instrucciones,
        p.created_at
        FROM publicaciones AS p, users AS u
        WHERE u.id=p.id_user and p.status='1' ");
        return Datatables::of($datos)->make(true);
    }

    public function data_publicacion(Request $request){
       return $datos=Publicaciones::where('id','=',$request->id)->get();
    }

    public function create_coment_user(Request $request){
        $nuevo_comment=new Comentario();
        $nuevo_comment->id_publicacion     =$request->id_publicacion;
        $nuevo_comment->id_usuario          =Auth::user()->id;
        $nuevo_comment->comentario          =$request->comentario;
        $nuevo_comment->save();
        return "bien";
    }

    public function data_publicacion_with_comments(Request $request){
        $comentarios=DB::SELECT('SELECT users.fullname,
                                        comentario.comentario,
                                        comentario.created_at
                                FROM users, comentario
                                WHERE users.id =comentario.id_usuario AND comentario.id_publicacion=?',[$request->id]);

        $datos=DB::SELECT('SELECT 
                                u.fullname AS publicador_name,
                                u.profile_picture,
                                p.titulo,
                                p.instrucciones,
                                p.material_adicional,
                                p.enlace,
                                p.created_at AS publicacion_time
                            FROM publicaciones AS p, users AS u
                            WHERE u.id=p.id_user  AND p.id=?',[$request->id]);
        $data=[$comentarios,$datos];
        return $data;
    }

    public function data_publicacion_with_comments_admin(Request $request){
        // $comentarios=Comentario::where('id_publicacion','=',$request->id)->get();
        $comentarios=DB::SELECT('SELECT users.fullname,
                                        comentario.comentario,
                                        comentario.created_at
                                FROM users, comentario
                                WHERE users.id =comentario.id_usuario AND comentario.id_publicacion=?',[$request->id]);

        $datos=DB::SELECT('SELECT 
                                u.fullname AS publicador_name,
                                u.profile_picture,
                                p.titulo,
                                p.instrucciones,
                                p.material_adicional,
                                p.enlace,
                                p.created_at AS publicacion_time
                            FROM publicaciones AS p, users AS u
                            WHERE u.id=p.id_user  AND p.id=?',[$request->id]);
        $data=[$comentarios,$datos];
        return $data;
    }
  
}
