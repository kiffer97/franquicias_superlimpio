<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
// use app\User;
use DB;
use App\User;
use App\Franquicia;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Hash;


// use Illuminate\Http\Request;

class FranquiciaController extends Controller
{

   public function cambiar_nav(Request $request){
    User::where('id','=',Auth::user()->id)->update(['nav_mode'=>$request->cambio]);
    return "bien";
   }
   public function tipo_nav(Request $request){
    return User::where('id','=',Auth::user()->id)->select(['nav_mode'])->get();
   }

   public function franquicias_view(Request $request){
    return view('franquicia.franquicia_view');
   }
   public function exist_mail(Request $request){
       $correo=count( User::where('email','=',$request->mail)->get());
       return $correo;
   }
   public function create_franquicia(Request $request){
      // echo $request;
      $nuevoUsuario= new User();
      $nuevoUsuario->name         =$request->user_name;
      $nuevoUsuario->first_name   =$request->ap_paterno;
      $nuevoUsuario->last_name    =$request->ap_materno;
      $nuevoUsuario->rol          ="Franquiciador";
      $nuevoUsuario->fullname     =$request->user_name." ".$request->ap_paterno." ".$request->ap_materno;
      $nuevoUsuario->nav_mode     =0;
      $nuevoUsuario->email        =$request->user_mail;
      $nuevoUsuario->password     =Hash::make($request->password);
      $nuevoUsuario->save();

      $nuevaFranquicia=new Franquicia();
      $nuevaFranquicia->nombre=$request->nombre;
      $nuevaFranquicia->franquiciador=$nuevoUsuario->id;
      $nuevaFranquicia->status=1;
      $nuevaFranquicia->save();
      return "bien";
  }

  public function update_franquicia(Request $request){

      Franquicia::where('id','=',$request->id)
                        ->update(["nombre"=>$request->nombre]);

      $id_user_franquiciador=Franquicia::where('id','=',$request->id)
                        ->select(["franquiciador"])->get();

        if(count($id_user_franquiciador)>0){

            if($request->password!=""){
                User::where('id','=',$id_user_franquiciador[0]->franquiciador)
                    ->update([
                            "name"          =>  $request->user_name,
                            "first_name"    =>  $request->first_name,
                            "last_name"     =>  $request->last_name,
                            "email"         =>  $request->mail,
                            "password"      =>Hash::make($request->password),
                            "fullname"      =>  $request->user_name." ".$request->first_name." ".$request->last_name
                            ]);
            }else{
                User::where('id','=',$id_user_franquiciador[0]->franquiciador)
                    ->update([
                            "name"          =>  $request->user_name,
                            "first_name"    =>  $request->first_name,
                            "last_name"     =>  $request->last_name,
                            "email"         =>  $request->mail,
                            "fullname"      =>  $request->user_name." ".$request->first_name." ".$request->last_name
                            ]);
            }
        }
    

      return "bien";
  }
  public function delete_franquicia(Request $request){
      Franquicia::where('id','=',$request->id)->update(["status"=>0 ]);
      return "bien";
  }

  public function franquicia_data_table()
  {
      $datos=Franquicia::where('status','=',1)->get();
      return Datatables::of($datos)->make(true);
      // return array($datos);
  }
  public function datos_one_franquicia(Request $request)
  {
     $datos=DB::SELECT('SELECT users.email, users.name,users.first_name,users.last_name,franquicia.nombre
                        FROM users,franquicia
                        WHERE franquicia.id=? AND franquicia.franquiciador=users.id;',[$request->id]);
      return $datos;
  }



  
}
