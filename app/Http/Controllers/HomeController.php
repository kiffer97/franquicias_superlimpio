<?php

namespace App\Http\Controllers;
use Auth;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->rol=="Administrador"){
            return view('franquicia.franquicia_view');
            
        }else if(Auth::user()->rol=="Franquiciador"){
            return view('publicaciones.publicaciones_view_user');

        }else{
            return view('home');
        }
    }
    public function perfil_usuario()
    {
        return view('perfil');
    }

    public function usuario_info(){
        return User::where('id','=',Auth::user()->id)->get();
    }
    public function update_perfil(Request $request){
        if($request->password==''){
            User::where('id','=',Auth::user()->id)
                ->update(
                        [
                            'name'      =>$request->nombre,
                            'first_name'=>$request->firstname,
                            'last_name' =>$request->lastname,
                            'fullname'  =>$request->nombre." ".$request->firstname." ".$request->lastname
                        ]
                );
        }else{
            User::where('id','=',Auth::user()->id)
                ->update(
                        [
                            'name'      =>$request->nombre,
                            'first_name'=>$request->firstname,
                            'last_name' =>$request->lastname,
                            'fullname'  =>$request->nombre." ".$request->firstname." ".$request->lastname,
                            'password'=>Hash::make($request->password)
                        ]
                );
        }
        return "bien";
    }
    public function update_imagen_perfil(Request $request){

        if($request->hasfile('imagen_perfil')){
            $file=$request->file('imagen_perfil');
            $nombre=time().".".$file->getClientOriginalExtension();
            $file->move(public_path().'/images/profile_user',$nombre);
           $nueva_imagen_perfil=$nombre; 
        }else{
           $nueva_imagen_perfil="";
        }

        User::where('id','=',Auth::user()->id)->update(['profile_picture'=>$nueva_imagen_perfil]);

        return $nombre;
    }

}
