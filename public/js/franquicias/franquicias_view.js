$(document).ready(function() {
	var datosEstudiantes=[];
	var ids_Estudiantes=[];

    function pintarTabla()
    {
		var htmlTabla=`
            <table id="datable_1" class="table table-bordered table-striped dataTable dtr-inline">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th style='width:40%;text-justify'>Nombre de la franquicia</th>
                        <th >Alta</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody >
                </tbody>
            </table>
			`;
		$(".table-wrap").html(htmlTabla);    
        $('#datable_1').DataTable({
            responsive: true,
			autoWidth: false,
			"order": [[ 0, "desc" ]],
            language: { search: "",
            searchPlaceholder: "Buscar franquicia",
            sLengthMenu: "_MENU_items"
            },
            ajax:'/franquicia_data_table',
            columns: [
                { data: 'id',  name: 'id' },
                { data: 'nombre',   name: 'nombre' },
                // { data: 'descripcion',   name: 'descripcion' }
                // { data: 'precio',   name: 'precio' }
                // { data: 'date_end',   name: 'date_end' }
			],
			columnDefs:[
					{"targets":2, "data":function(data,type,full,meta){
						return data.created_at.slice(0,10);
					}
            	},
				{"targets":3, "data":function(data,type,full,meta){
						var htmlBotones=``;
						// htmlBotones+=`<button id="${data.id}"  
						// 				data-toggle="tooltip-primary" data-placement="top" title="Agregar nueva clase" data-content="Este botón permite crear una clase para el curso: ${data.name}."
						// 				class="btn_nueva_clase btn btn-success btn-rounded btn-outline-success">
						// 				<i class="fa fa-plus" aria-hidden="true"></i>
						// 				</button>`;
						htmlBotones+=`<button id="${data.id}" 
										data-toggle="tooltip-primary" data-placement="top" title="Eliminar" data-content=""
										class="btn-eliminar-producto btn btn-danger ml-1 mr-1  ">
                                        <i class="fa fa-trash" aria-hidden="true"></i>
									</button>`;
						htmlBotones+=`<button id="${data.id}" 
											data-toggle="tooltip-primary" data-placement="top" title="Editar" data-content="."
											class="btn-editar-producto btn btn-primary ">
											<i class="fas fa-edit"></i>
										</button>`;
						// htmlBotones+=`<a href='/cursos-${data.id}' id="${data.id} target="_blank"
						// 				data-toggle="tooltip-primary" data-placement="top" title="Cursos" data-content=."
						// 				class=" btn btn-light ml-1 btn-rounded btn-outline-light">
						// 				<i class="fa fa-list" aria-hidden="true"></i>
						// 			</a>`;
						// htmlBotones+=`<a href="/usuarios/${data.id}" 
						// 				data-toggle="tooltip-primary" data-placement="top" title="Usuarios activos" data-content=""
						// 				class="btn btn-success ml-1 btn-rounded btn-outline-success">
						// 				<i class="fa fa-user-plus" aria-hidden="true"></i>
						// 			</a>`;
						// htmlBotones+=`<a href="/usuarios-deshabilitados/${data.id}" 
						// 				data-toggle="tooltip-primary" data-placement="top" title="Usuarios inactivos" data-content=""
						// 				class="btn btn-warning ml-1 btn-rounded btn-outline-warning">
						// 				<i class="fa fa-user-times" aria-hidden="true"></i>
						// 			</a>`;
						// htmlBotones+=`<button type="button" class="btn btn-primary" data-toggle="tooltip-primary" data-placement="top" title="Tooltip on top">Tooltip Primary</button>`;
						return htmlBotones;
					}
				}

			],
			"drawCallback": function( settings ) {
				// $(".btn_nueva_clase,.btn-eliminar-producto, .btn-editar-producto, .btn_nuevo_clases_estudiantes").mouseover(function(){
				// 	$(this).popover('show');
				// }).mouseout(function(){
				// 	$(this).popover('hide');
				// });
				apagar_eventos();
				eventos();
			}
        });
	}//end pintarTabla()
	pintarTabla();

	function confirmacion(leyenda,id){
		Swal.fire({
			title: '¿Estas seguro de eliminar?',
			text:leyenda,
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar'
		  }).then((result) => {
			if (result.value) {
					$.ajax({
	                method:'GET',
	                url:"/delete_franquicia",
	                data:{
	                'id':id
	                }
	            })
	            .done(function(respuesta1){    
					pintarTabla();
                    alertProceso('Franquicia eliminada correctamente!');
	            });
			
			}
		  })
	}
	function alertProceso(leyenda){
		Swal.fire({
			position: 'top',
			icon: 'success',
			title: leyenda,
			showConfirmButton: false,
			timer: 1500
		  })
	}
	function alertAgregaClase(){
		Swal.fire(
			'Opss',
			'Debes crear una clase para este curso, antes de usar esta opción!',
			'error'
		  )
	}
	function validacion(identificador,alert){
		console.log('ejecutando validacion()');
		let contador =0;
		htmlIDValidacion=document.querySelectorAll(identificador);
		console.log(htmlIDValidacion);

		htmlIDValidacion.forEach(function(valor,index){

			if(valor.value==""){
				contador++;
				$(`#${valor.id}`).addClass('is-invalid')
			}else{
				$(`#${valor.id}`).removeClass('is-invalid')
			}
		});

		if(contador==0){
			$(alert).addClass('d-none')
		}else{
			$(alert).removeClass('d-none')
		}

		console.log(contador);

		return contador;
	}

	function limpiar_validacion(identificador,alert){
		console.log('ejecutando limpiar_validacion()');

		htmlIDValidacion=document.querySelectorAll(identificador);

		htmlIDValidacion.forEach(function(valor,index){
			$(`#${valor.id}`).removeClass('is-invalid')
		});

		$(alert).addClass('d-none')
	}    



	
	function eventos(){
	        $('.btn-modal-new-producto').click(function(){
					limpiar_validacion('.validacion_create','#i_alert_validacion_create');
					$('#inombre').val("");
	                $('#i_user_name').val("");
	                $("#i_user_ap1").val("");
	                $("#i_user_ap2").val("");
	                $("#i_user_mail").val("");
	                $("#i_user_password").val("");
				
	            $('#modal_clase_usuario').modal("show");
			});
			
	        $('.btn_guardar_new_producto').click(function(){
				contador=0;
				nombre=			$('#inombre').val();
				user_name=		$('#i_user_name').val();
				ap_paterno=		$('#i_user_ap1').val();
				ap_materno=		$('#i_user_ap2').val();
				user_mail=		$('#i_user_mail').val();
				password=		$('#i_user_password').val();

			

				if(validacion('.validacion_create','#i_alert_validacion_create')==0){
					$.ajax({
					    method:'GET',
					    url:"/create_franquicia",
					    data:{
								nombre:nombre,
								user_name:user_name,
								ap_paterno:ap_paterno,
								ap_materno:ap_materno,
								user_mail:user_mail,
								password:password
							}
					})
					.done(function(respuesta1){                
						// console.log(respuesta1);
						if(respuesta1=="bien")
						{
							$('#modal_clase_usuario').modal("hide");
							pintarTabla();
							alertProceso('Franquicia registrada con exito!');
						}
						
					});//end .done
				}
			})//end function


			$(".btn-editar-producto").click(function(){
				limpiar_validacion('.validacion_update','#i_alert_validacion_edit');
				var id=$(this).attr("id");
				console.log("actualizar franquicia");
				console.log(id);
				$.ajax({
	                method:'GET',
	                url:"/datos_one_franquicia",
	                data:{
	                'id':id
	                }
	            })
	            .done(function(respuesta1){    

					console.log(respuesta1);
					$('.verifica-valid-edit').each(function(){
						var id=$(this).attr("id");
						$('#'+id+' p').remove();
                    });
                    
					$('#i_nombre_edit').val(respuesta1[0].nombre);
					$('#i_edit_user_name').val(respuesta1[0].name);
					$('#i_edit_user_ap1').val(respuesta1[0].first_name);
					$('#i_edit_user_ap2').val(respuesta1[0].last_name);
					$('#i_edit_user_mail').val(respuesta1[0].email);
					$('#i_edit_user_password').val("");
	                // $("#idescripcion_edit").val(respuesta1[0].descripcion);
	                // $("#iprecio_edit").val(respuesta1[0].precio);
					$("#id_curso").val(id);
					// $("#ihoras_edit").val(respuesta1[0].horas_duracion);
	                // $("#igiro_edit").val(respuesta1[0].giro);
	                // $("#i_select_validez_edit").val(respuesta1[0].date_end);
            });
                 $('#modal_editar_producto').modal('show');
			})

			$("#btn_guardar_edit_producto").click(function(){
                if(validacion('.validacion_update','#i_alert_validacion_edit')==0){
					$.ajax({
						method:'GET',
						url:"/update_franquicia",
						data:{
							'id':$("#id_curso").val(),
							'nombre':$('#i_nombre_edit').val(),
							'user_name':$('#i_edit_user_name').val(),
							'first_name':$('#i_edit_user_ap1').val(),
							'last_name':$('#i_edit_user_ap2').val(),
							'mail':$('#i_edit_user_mail').val(),
							'password':$('#i_edit_user_password').val(),
						}
					})
					.done(function(respuesta1){                
						// console.log(respuesta1);
						if(respuesta1=="bien")
						{
							$('#modal_editar_producto').modal("hide");
							pintarTabla();
							alertProceso('Franquicia actualiza correctamente!');
						}
						
					});
				}
			});

			$(".btn-eliminar-producto").click(function(){
				var id=$(this).attr("id");
				
				console.log("btn eliminar");
				console.log(id);
				confirmacion('Se eliminará la fraquicia: '+id,id);
            });
            
			$(".btn_nueva_clase").click(function(){
				var id=$(this).attr('id');
				// console.log(id);
				$("#id_producto_elim").val(id);

				$.ajax({
	                method:'GET',
	                url:"/existeClase",
	                data:{
					'id_curso':id
	                }
	            })
	            .done(function(respuesta1){                
	                if(respuesta1=='existe'){
							$.ajax({
							method:'GET',
							url:"/consultarClase",
							data:{
							'id_curso':id
							}
							})
							.done(function(respuesta1){                
								console.log(respuesta1);
								$("#id_producto_elim").val();
								$("#i_url").val(respuesta1[0].url);
								$("#i_class_begin").val(respuesta1[0].date_begin);
								$("#i_select_estatus_clase").val(respuesta1[0].status);
							});	//end .done
					}else{
						$("#i_url").val("");
						$("#i_class_begin").val("");
					}
				});//end .done

				$("#modal_eliminar_producto").modal("show");
			});

			$(".btn_guardar_elim_producto").click(function(){

				console.log($("#id_producto_elim").val());
				console.log($("#i_url").val());
				console.log($("#i_class_begin").val());
				console.log($("#i_select_estatus_clase").val());

				$.ajax({
	                method:'GET',
	                url:"/guardarNuevaClase",
	                data:{
					'id_curso':$("#id_producto_elim").val(),
					'i_url':$("#i_url").val(),
					'i_class_begin':$("#i_class_begin").val(),
					'i_select_estatus_clase':$("#i_select_estatus_clase").val()
	                }
	            })
	            .done(function(respuesta1){                
	                console.log(respuesta1);
	                $('#modal_eliminar_producto').modal("hide");
	               pintarTabla();
	            });
			});

			$(".btn_nuevo_clases_estudiantes").click(function(){

				var id=$(this).attr('id');
				//obtengo el id del curso actual en el modal
                $("#modal_id_curso").val(identificador_curso);
				$("#modal_clase_usuario").modal('show');

				
				
				// });//end .done

			});// end btn_nuevo_clases_estudiantes

			$(".btn_guardar_cambios_class_estud").click(function(){
				var id=$("#modal_id_curso").val();

				console.log(id);
				console.log(ids_Estudiantes);
				
				$.ajax({
	                method:'GET',
	                url:"/guardarCursoUsuarios",
	                data:{
					'id_curso':id,
					'estudiantes':ids_Estudiantes
	                }
	            })
	            .done(function(respuesta1){  
					if(respuesta1=="bien"){
						alertProceso('Usuario añadidos exitosamente!');
					}        
					pintarTabla();
					// se limpian los datos de la ejecucion de agregar usuario
					selectEstudiantes();
					ids_Estudiantes=[];
					$("#tabla_estudiantes").addClass('d-none');
					$("#i_tbody tr").remove();
					

				$("#modal_clase_usuario").modal('hide');
				});//end .done

			});// end btn_guardar_cambios_class_estud
	}//end eventos

	function apagar_eventos(){
	
		$('.btn_eliminar_estudiante').off("click");
		$('.btn_nuevo_clases_estudiantes').off("click");
		$('.btn-modal-new-producto').off("click");
		$('.btn_guardar_new_producto').off("click");
		$('.btn-editar-producto').off("click");
		$('#btn_guardar_edit_producto').off("click");
		$('.btn-eliminar-producto').off("click");
		$('.btn_guardar_elim_producto').off("click");
		$('.btn_guardar_cambios_class_estud').off("click");

	}

   

 });// end document.ready function