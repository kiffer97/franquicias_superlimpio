$(document).ready(function() {

    console.log('ejecuntando publicaiones');
	// console.log(id_curso_heredado);
	let actualizarFile;
	var datosEstudiantes=[];
	var ids_Estudiantes=[];

    function pintarTabla()
    {
		var htmlTabla=`
            <table width="100%" id="datable_1" class="  dataTable dtr-inline">
                <thead>
                    <tr>
                        <th></th>
                        <th>Publicaciones</th>
                    </tr>
                </thead>
                <tbody >
                </tbody>
            </table>
			`;
		$(".table-wrap").html(htmlTabla);    
        $('#datable_1').DataTable({
			responsive: true,
			"order": [[ 0, "desc" ]],
            autoWidth: false,
            language: { search: "",
            searchPlaceholder: "Buscar publicación",
            sLengthMenu: "_MENU_items"
            },
            ajax:`/datatable_publicaciones`,
            columns: [
                // { data: 'id',  name: 'id' },
                // { data: 'nombre',   name: 'nombre' },
                // { data: 'descripcion',   name: 'descripcion' }
			],
			columnDefs:[
               
				{"targets":0, "data":function(data,type,full,meta){
						
						return `<p class="d-none">${data.id}</p>`;
					}
				},
				{"targets":1, "data":function(data,type,full,meta){
						var htmlCards=``;
                                    
						// htmlCards+=`<button id="${data.id}" 
						// 					data-toggle="tooltip-primary" data-placement="top" title="Editar" data-content="."
						// 					class="btn-editar-producto btn btn-blue btn-rounded btn-outline-blue">
						// 					<i class="fa fa-pencil-square-o" aria-hidden="true">
                        // 				</i></button>`;
                        htmlCards+=`
                                    <div class="card">
                                        <div class="card-content">
											<div class="row">
											
                                                <div class="col-md-1">
                                                    <h5 class="ml-4 mt-3">#</h5>
												</div>
												
                                                <div class="col-md-9 btn_abrir_publicacion" id="${data.id}" style="cursor:pointer">
                                                    <h5 class="">${data.titulo}</h5>
                                                    <p>${data.instrucciones.slice(0,70)}...</p>
												</div>
												
												<div class="col-md-2 text-center">
													<li class="nav-item dropdown " style="list-style:none">
														<a href="#" class="nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
															<i  class="fa fa-ellipsis-v mt-4" aria-hidden="true" style="cursor:pointer"></i>
														</a>
														<div class="dropdown-menu " aria-labelledby="navbarDropdown">
															<a class="dropdown-item btn-editar-producto" id="${data.id}" style="cursor:pointer">
																<i class="fas fa-edit"></i>
																Editar
															</a>
															<a class="dropdown-item btn-eliminar-producto" id="${data.id}" style="cursor:pointer">
																<i class="far fa-trash-alt"></i>
																Eliminar
																</a>
														</div>
													</li>
												</div>
												
                                            </div>
                                        </div>
                                    </div>

                                    `;
						return htmlCards;
					}
				}

			],
			"drawCallback": function( settings ) {
				// $(".btn_nueva_clase,.btn-eliminar-producto, .btn-editar-producto, .btn_nuevo_clases_estudiantes").mouseover(function(){
				// 	$(this).popover('show');
				// }).mouseout(function(){
				// 	$(this).popover('hide');
				// });
				apagar_eventos();
				eventos();
			}
        });
	}//end pintarTabla()
	pintarTabla();

	function confirmacion(leyenda,id){
		Swal.fire({
			title: '¿Estas seguro de eliminar?',
			text:leyenda,
			icon: 'question',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Si, eliminar',
			cancelButtonText: 'Cancelar'
		  }).then((result) => {
			if (result.value) {
					$.ajax({
	                method:'GET',
	                url:"/delete_publicacion",
	                data:{
	                'id':id
	                }
	            })
	            .done(function(respuesta1){    
					pintarTabla();
                    alertProceso('Publicación eliminada correctamente!');
	            });
			
			}
		  })
	}
	function alertProceso(leyenda){
		Swal.fire({
			position: 'top',
			icon: 'success',
			title: leyenda,
			showConfirmButton: false,
			timer: 1500
		  })
	}

	function validacion(identificador,alert){
		console.log('ejecutando validacion()');
		let contador =0;
		htmlIDValidacion=document.querySelectorAll(identificador);
		console.log(htmlIDValidacion);

		htmlIDValidacion.forEach(function(valor,index){

			if(valor.value==""){
				contador++;
				$(`#${valor.id}`).addClass('is-invalid')
			}else{
				$(`#${valor.id}`).removeClass('is-invalid')
			}
		});

		if(contador==0){
			$(alert).addClass('d-none')
		}else{
			$(alert).removeClass('d-none')
		}

		console.log(contador);


		return contador;
	}

	function limpiar_validacion(identificador,alert){
		console.log('ejecutando limpiar_validacion()');

		htmlIDValidacion=document.querySelectorAll(identificador);

		htmlIDValidacion.forEach(function(valor,index){
			$(`#${valor.id}`).removeClass('is-invalid')
		});

		$(alert).addClass('d-none')
	}
	function pintar_publicacion(){
		console.log('pintar_publicacion');
		//cada vez que se abre se limpia
		$('.i_publicador')				.html('');
		$('.i_shared_time')				.html('');
		$('.title_publicacion')			.html('');
		$('.i_descricion_publicacion')	.html('');
		$('#content_material')			.html('');
		$('#content_comentarios')		.html('');	
		$('.i_publicador_image')		.attr('src','');
						


		$.ajax({
			method:'GET',
			url:"/data_publicacion_with_comments_admin",
			data:{
					'id':id_publicacion,
				}
		})
		.done(function(respuesta1){        
			comentario=respuesta1[0];        
			publicacion=respuesta1[1]; 

			console.log(respuesta1);
			//se pinta la info
			$('.i_publicador')				.html(publicacion[0].publicador_name);
			$('.i_shared_time')				.html(formato_fecha(publicacion[0].publicacion_time));
			$('.title_publicacion')			.html(publicacion[0].titulo);
			$('.i_descricion_publicacion')	.html(publicacion[0].instrucciones);
			$('.i_publicador_image')		.attr('src','/images/profile_user/'+publicacion[0].profile_picture);


			if(publicacion[0].material_adicional){
				html_material=``;
				html_material+=`
							<tr>
								<th>Material</th>
								<th>
									<a href="/fichero_material_publicaciones/${publicacion[0].material_adicional}" download >
									<i class="fa fa-download" aria-hidden="true"></i>
									${publicacion[0].material_adicional} 
									</a>
								</th>
							</tr>
								`;
				$('#content_material').append(html_material);
			}

			if(publicacion[0].enlace){
				html_enlace=``;
				html_enlace+=`
							<tr>
								<th>Enlace</th>
								<th>
									<a href="${publicacion[0].enlace}" target="_blank" style="color:#007bff">
										<i class="fa fa-link" aria-hidden="true"></i> 
										${publicacion[0].enlace}
									</a>
								</th>
							</tr>
							`;
				$('#content_material').append(html_enlace);
			}

			html_comentario=``;

			comentario.forEach(function(valor,index){
				if(valor.comentario){
					hora=formato_hora(valor.created_at);
					html_comentario+=
					`
					<!-- timeline item -->
					<div>
						<i class="fas fa-envelope bg-primary"></i>
						<div class="timeline-item">
							<span class="time"><i class="far fa-clock"></i> ${hora}</span>
							<div class="timeline-body" style="min-width:400px">
								<h6 class="timeline-header"><a href="#" style="font-weight: 600;">${valor.fullname}</a> </h6>
								${valor.comentario}
							</div>
						</div>
					</div>
					<!-- END timeline item -->
					`;
				}
			})
			$('#content_comentarios').append(html_comentario);					

		});//end .done

		
	}//end function
	function formato_fecha(fecha){
		//2020-08-28
		string_año=fecha.slice(0,4);
		string_mes=fecha.slice(5,7);
		string_dia=fecha.slice(8,10);

		meses=[
				'',
				'Enero',
				'Febrero',
				'Marzo',
				'Abril',
				'Mayo',
				'Junio',
				'Julio',
				'Agosto',
				'Septiembre',
				'Octubre',
				'Noviembre',
				'Diciembre',
			];
		nuevo_formato_fecha=string_dia+' '+meses[parseInt(string_mes)]+' '+string_año;
		return nuevo_formato_fecha;
	}
	function formato_hora(fecha){
		// 2020-08-28T22:29:07.000000Z

		string_fecha=fecha+"";
		//tiempo=22:29:07
		tiempo=string_fecha.slice(11,19);
		
		hora=parseFloat( tiempo.slice(0,2));
		minutos=tiempo.slice(3,5);

		if(hora>12){
			siglas='pm';
			hora_corregida=hora-12;
		}else{
			siglas='am';
			hora_corregida=hora;
		}
		// console.log(hora_corregida+":"+minutos+':'+siglas);
		nuevo_formato=hora_corregida+":"+minutos+' '+siglas;
		return nuevo_formato;

	}
	
	function eventos(){
		$('.btn_abrir_publicacion').click(function(){
			id_publicacion=0;
			id_publicacion=$(this).attr('id');
			console.log('id_publicacion: '+id_publicacion);
			pintar_publicacion();
			$('#modal_vista_publicacion').modal('show');


		});
	        $('.btn-modal-new-producto').click(function(){
					limpiar_validacion('.validacion_create','#i_alert_campos_vacios1');
					$('#inombre').val("");
	                $('#idescripcion').val("");
	                $('#i_fecha_limite').val("");
	                // $("#iprecio").val("");
	                // $("#ihoras").val("");
	                // $("#igiro").val("");
				
	            $('#modal_clase_usuario').modal("show");
            });
            
	        $('.btn_guardar_new_producto').click(function(){
				nombre          =$('#inombre').val();
				descripcion     =$('#idescripcion').val();
				fecha_limite  =$('#i_fecha_limite').val();

	            $.ajax({
	                method:'GET',
	                url:"/create_publicacion",
	                data:{
							'inombre':nombre,
							'idescripcion':descripcion,
							'i_fecha_limite':fecha_limite,
						}
	            })
	            .done(function(respuesta1){                
					// console.log(respuesta1);
					if(respuesta1=="bien")
					{
						$('#modal_clase_usuario').modal("hide");
						pintarTabla();
						alertProceso('Tarea guardada exitosamente!');
					}
					
				});//end .done
				
            })//end function
            
			$(".btn-editar-producto").click(function(){
				limpiar_validacion('.validacion_edit','#i_alert_campos_vacios');
				$("#i_edit_file").val('');
				
				var id=$(this).attr("id");
				actualizarFile=0;
				$('#eliminar_file').val(actualizarFile);
				$('#i_mostrar_input_file').addClass('d-none');
				$('#contenedor_file_eliminar2').html(``);

				console.log("actualizar tarea");
				console.log(id);
				$.ajax({
	                method:'GET',
	                url:"/data_publicacion",
	                data:{
	                'id':id
	                }
	            })
	            .done(function(respuesta1){                
                    console.log(respuesta1);
					$('#i_nombre_edit').val(respuesta1[0].titulo);
	                $("#idescripcion_edit").val(respuesta1[0].instrucciones);
	                $("#i_edit_fecha_limite").val(respuesta1[0].fecha_entrega);
					$("#id_curso").val(id);
					$("#i_edit_enlace").val(respuesta1[0].enlace);

					if(respuesta1[0].material_adicional!="" && respuesta1[0].material_adicional!=null ){
						// console.log('');
						let htmlEliminarFile=`
						<label  class="col-sm-2 col-form-label" id="elim_label">Material adicional: </label>
						<div class="col-sm-6" id="i_contenedor_card_elimin_file">
							<div class="card mt-2">
								<div class="card-content mt-2 mx-2">
									<table width="100%">
										<tr>
											<td	width="10%">
												<i class="fa fa-file ml-2" aria-hidden="true"></i>
											</td>
											<td	width="80%">
												<p class="mb-2">${respuesta1[0].material_adicional}</p>
											</td>
											<td	width="10%">
											<a class="inline-block mt-3 btn_eliminar_file btn_eliminar_archivo" href="#"  >
												<i class="fas fa-times-circle text-danger mt-1"></i>
											</a>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
						`;
						$('#contenedor_file_eliminar2').html(htmlEliminarFile);

						$('.btn_eliminar_archivo').click(function(){
							$('#i_mostrar_input_file').removeClass('d-none');
							actualizarFile=1;
							$('#i_contenedor_card_elimin_file').remove();
							$('#elim_label').remove();
							$('#eliminar_file').val(actualizarFile);
						});
					}else{
						$('#i_mostrar_input_file').removeClass('d-none');
					}

			});
				 $('#modal_editar_producto').modal('show');
				 
				// validacion('.validacion_edit');
            })
            
			$("#btn_guardar_edit_producto").click(function(){
				console.log("here4");
				// console.log($("#id_curso").val());
				// console.log($("#i_nombre_edit").val());
				// console.log($("#idescripcion_edit").val());
				// console.log($("#iprecio_edit").val());
				// console.log($("#id_curso").val());

				// var hoy = new Date();

				// var dd = hoy.getDate();
				// var mm = hoy.getMonth()+1;
				// var año = hoy.getFullYear();

				// validez=$('#i_select_validez_edit').val();
				
				// if(mm<10){
				// 	mm="0"+mm;
				// }
				
				// if(dd<10){
				// 	dd="0"+dd;
				// }
				// if(validez=="1"){
				// 	año=año+1;
				// }else{
				// 	año=año+2;
				// }

				// console.log(año);
				
				// var fecha=dd+"-"+mm+"-"+año;
				// console.log(fecha);
				
				$.ajax({
	                method:'GET',
	                url:"/update_tarea_maestro",
	                data:{
						'id':$("#id_curso").val(),
						'nombre':$('#i_nombre_edit').val(),
						'descripcion':$("#idescripcion_edit").val(),
						'fecha_limite':$("#i_edit_fecha_limite").val()
	                }
	            })
	            .done(function(respuesta1){                
	                // console.log(respuesta1);
					if(respuesta1=="bien")
					{
						$('#modal_editar_producto').modal("hide");
						pintarTabla();
						alertProceso('Tarea actualizada correctamente!');

					}
					
	            });
			});

			$(".btn-eliminar-producto").click(function(){
				var id=$(this).attr("id");
				
				console.log("btn eliminar");
				console.log(id);
				confirmacion('Se eliminará la publicación: '+id,id);
            });
            
			$(".btn_nueva_clase").click(function(){
				var id=$(this).attr('id');
				// console.log(id);
				$("#id_producto_elim").val(id);

				$.ajax({
	                method:'GET',
	                url:"/existeClase",
	                data:{
					'id_curso':id
	                }
	            })
	            .done(function(respuesta1){                
	                if(respuesta1=='existe'){
							$.ajax({
							method:'GET',
							url:"/consultarClase",
							data:{
							'id_curso':id
							}
							})
							.done(function(respuesta1){                
								console.log(respuesta1);
								$("#id_producto_elim").val();
								$("#i_url").val(respuesta1[0].url);
								$("#i_class_begin").val(respuesta1[0].date_begin);
								$("#i_select_estatus_clase").val(respuesta1[0].status);
							});	//end .done
					}else{
						$("#i_url").val("");
						$("#i_class_begin").val("");
					}
				});//end .done

				$("#modal_eliminar_producto").modal("show");
			});

			$(".btn_guardar_elim_producto").click(function(){

				console.log($("#id_producto_elim").val());
				console.log($("#i_url").val());
				console.log($("#i_class_begin").val());
				console.log($("#i_select_estatus_clase").val());

				$.ajax({
	                method:'GET',
	                url:"/guardarNuevaClase",
	                data:{
					'id_curso':$("#id_producto_elim").val(),
					'i_url':$("#i_url").val(),
					'i_class_begin':$("#i_class_begin").val(),
					'i_select_estatus_clase':$("#i_select_estatus_clase").val()
	                }
	            })
	            .done(function(respuesta1){                
	                console.log(respuesta1);
	                $('#modal_eliminar_producto').modal("hide");
	               pintarTabla();
	            });
			});

			$(".btn_nuevo_clases_estudiantes").click(function(){

				var id=$(this).attr('id');
				//obtengo el id del curso actual en el modal
                $("#modal_id_curso").val(identificador_curso);
				$("#modal_clase_usuario").modal('show');

				
				
				// });//end .done

			});// end btn_nuevo_clases_estudiantes

			$(".btn_guardar_cambios_class_estud").click(function(){
				var id=$("#modal_id_curso").val();

				console.log(id);
				console.log(ids_Estudiantes);
				
				$.ajax({
	                method:'GET',
	                url:"/guardarCursoUsuarios",
	                data:{
					'id_curso':id,
					'estudiantes':ids_Estudiantes
	                }
	            })
	            .done(function(respuesta1){  
					if(respuesta1=="bien"){
						alertProceso('Usuario añadidos exitosamente!');
					}        
					pintarTabla();
					// se limpian los datos de la ejecucion de agregar usuario
					selectEstudiantes();
					ids_Estudiantes=[];
					$("#tabla_estudiantes").addClass('d-none');
					$("#i_tbody tr").remove();
					

				$("#modal_clase_usuario").modal('hide');
				});//end .done

			});// end btn_guardar_cambios_class_estud
	}//end eventos

	function apagar_eventos(){
	
		$('.btn_eliminar_estudiante').off("click");
		$('.btn_nuevo_clases_estudiantes').off("click");
		$('.btn-modal-new-producto').off("click");
		$('.btn_guardar_new_producto').off("click");
		$('.btn-editar-producto').off("click");
		$('#btn_guardar_edit_producto').off("click");
		$('.btn-eliminar-producto').off("click");
		$('.btn_guardar_elim_producto').off("click");
		$('.btn_guardar_cambios_class_estud').off("click");

	}

	$("#i_form_guardar_tarea_maestro").on("submit", function(e){
		e.preventDefault();
		var f = $(this);
		var formData = new FormData(document.getElementById("i_form_guardar_tarea_maestro"));
		// formData.append("dato", "valor");

		if(validacion('.validacion_create','#i_alert_campos_vacios1')==0){
	
			$.ajax({
				url: "/create_publicacion",
				type: "post",
				dataType: "html",
				data: formData,
				cache: false,
				contentType: false,
				processData: false
			})
			.done(function(res){
				if(res=="bien")
					{
						$('#modal_clase_usuario').modal("hide");
						pintarTabla();
						alertProceso('Publicación guardada exitosamente!');
					}
			});//end .done
		}//end if
	});// end i_form_guardar_tarea_maestro


	$("#i_form_edit_type_file").on("submit", function(e){
		e.preventDefault();
		var f = $(this);
		var formData = new FormData(document.getElementById("i_form_edit_type_file"));
		// formData.append("dato", "valor");

		if(validacion('.validacion_edit','#i_alert_campos_vacios')==0){
			
			$.ajax({
				url: "/update_publicacion",
				type: "post",
				dataType: "html",
				data: formData,
				cache: false,
				contentType: false,
				processData: false
			})
				.done(function(res){
					if(res=="bien")
					{
						$('#modal_editar_producto').modal("hide");
						pintarTabla();
						alertProceso('Publicación actualizada correctamente!');
					}
				});
		}//end if
	});

   

 });// end document.ready function