console.log('usuario perfil');
let tamaño_archivo_imagen=0;
pintar_info();


$('.btn_update_perfil').off('click'); 
$('.btn_update_perfil').click(function(){
    if(validacion('.validacion_update','#i_alert_campos_vacios1')==0){

        $.ajax({
            method:'GET',
            url:"/update_perfil",
            data:{
                nombre:         $('#i_nombre')      .val(),
                firstname:      $('#i_firstname')   .val(),
                lastname:       $('#i_lastname')    .val(),
                password:       $('#i_password')    .val()
            }
        })
        .done(function(r){        
           if(r=='bien'){
               pintar_info;
               Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Tu información fué actualizada!',
                showConfirmButton: false,
                timer: 1500
              });
            $('.profile-username').html( $('#i_nombre').val()+' '+$('#i_firstname').val()+' '+$('#i_lastname').val() );
            $('#i_password').val('');

           }
        });

    }
});

$('.btn_abrir_modal_imagen').off('click');
$('.btn_abrir_modal_imagen').click(function(){
console.log('subiendo');
$('#i_subir_imagen_modal').modal('show');
})

$('#imagen_perfil').bind('change', function() {
    tamaño_archivo_imagen= this.files[0].size;
});

$("#i_form_subir_imagen").on("submit", function(e){
    e.preventDefault();
    var f = $(this);
    var formData = new FormData(document.getElementById("i_form_subir_imagen"));
    // formData.append("dato", "valor");
    console.log('btn surbuir imaen');
        console.log($('#imagen_perfil').val());
        console.log(tamaño_archivo_imagen);

    if($('#imagen_perfil').val()!="" && tamaño_archivo_imagen<5000000){
        
        $.ajax({
            url: "/update_imagen_perfil",
            type: "post",
            dataType: "html",
            data: formData,
            cache: false,
            contentType: false,
            processData: false
        })
            .done(function(res){
                console.log(res);
                    $(".i_imagen_perfil_principal").attr("src","/images/profile_user/"+res);
            });
            $('#i_subir_imagen_modal').modal("hide");
            $('#imagen_perfil').val('');
    }//end if
});

function validacion(identificador,alert){
    console.log('ejecutando validacion()');
    let contador =0;
    htmlIDValidacion=document.querySelectorAll(identificador);
    console.log(htmlIDValidacion);

    htmlIDValidacion.forEach(function(valor,index){

        if(valor.value==""){
            contador++;
            $(`#${valor.id}`).addClass('is-invalid')
        }else{
            $(`#${valor.id}`).removeClass('is-invalid')
        }
    });

    if(contador==0){
        $(alert).addClass('d-none')
    }else{
        $(alert).removeClass('d-none')
    }

    console.log(contador);


    return contador;
}
function pintar_info(){
    $.ajax({
        method:'GET',
        url:"/usuario_info",
        data:{}
    })
    .done(function(r){        
        console.log(r);      
        $('#i_nombre')      .val(r[0].name       );
        $('#i_firstname')   .val(r[0].first_name );
        $('#i_lastname')    .val(r[0].last_name  );
        $('#i_mail')        .val(r[0].email      );
        $('.i_rol')         .html(r[0].rol       );
    });
}